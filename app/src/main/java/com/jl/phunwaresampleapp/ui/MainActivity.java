package com.jl.phunwaresampleapp.ui;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import com.google.gson.Gson;

import com.jl.phunwaresampleapp.R;
import com.jl.phunwaresampleapp.model.Venue;


public class MainActivity extends ActionBarActivity implements
        VenueListFragment.OnVenueSelectedListener{

    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.venue_detail_container) != null) {
            mTwoPane = true;
            ((VenueListFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.venue_list))
                    .setActivateOnItemClick(true);
        }
    }

    @Override
    public void onVenueSelected(Venue item) {

        if (mTwoPane) {
            VenueDetailFragment detailFragment = VenueDetailFragment.newInstance(item);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.venue_detail_container, detailFragment)
                    .commit();
        } else {
            Intent detailIntent = new Intent(this, DetailActivity.class);
            detailIntent.putExtra(VenueDetailFragment.ARG_VENUE, new Gson().toJson(item));
            startActivity(detailIntent);
        }
    }
}
