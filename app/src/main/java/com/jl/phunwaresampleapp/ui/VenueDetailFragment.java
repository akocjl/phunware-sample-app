package com.jl.phunwaresampleapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jl.phunwaresampleapp.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import com.jl.phunwaresampleapp.model.ScheduleItem;
import com.jl.phunwaresampleapp.model.Venue;

public class VenueDetailFragment extends Fragment {

    public static final String ARG_VENUE = "venue";

    private ImageView mImageView;
    private TextView mNameField;
    private TextView mAddressField;
    private TextView mScheduleField;
    private TextView mMessageField;
    private ProgressBar mProgressBar;

    private Venue mVenue;

    public VenueDetailFragment() {

    }

    public static VenueDetailFragment newInstance(Venue venue) {
        VenueDetailFragment frag = new VenueDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_VENUE, new Gson().toJson(venue));
        frag.setArguments(bundle);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        String jsonString = getArguments().getString(ARG_VENUE);
        mVenue = new Gson().fromJson(jsonString, Venue.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_venue_detail, container, false);
        mImageView = (ImageView) rootView.findViewById(R.id.imageView);
        mNameField = (TextView) rootView.findViewById(R.id.nameField);
        mAddressField = (TextView) rootView.findViewById(R.id.addressField);
        mScheduleField = (TextView) rootView.findViewById(R.id.scheduleField);
        mMessageField = (TextView) rootView.findViewById(R.id.messageField);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        mNameField.setText(mVenue.getName());
        mAddressField.setText(mVenue.getFullAddressWithZip(getActivity()));

        if (mVenue.getImageUrl() != null && !TextUtils.isEmpty(mVenue.getImageUrl())) {
            mProgressBar.setVisibility(View.VISIBLE);
            Picasso.with(getActivity()).load(mVenue.getImageUrl()).into(mImageView, new Callback() {
                @Override
                public void onSuccess() {
                    mProgressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    mMessageField.setText(R.string.failed_to_load_image);
                    mMessageField.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);
                }
            });
        } else {
            mMessageField.setVisibility(View.VISIBLE);
        }


        if (mVenue.getSchedule().size() > 0) {

            StringBuffer sb = new StringBuffer();
            for (ScheduleItem item: mVenue.getSchedule()) {
                sb.append(item.getSchedule());
                sb.append("\n");
            }

            mScheduleField.setText(sb.toString().trim());
        }

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_detail, menu);

        MenuItem item = menu.findItem(R.id.menu_item_share);
        ShareActionProvider mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);

        String shareString = getActivity().getString(R.string.share_formatter,
                mVenue.getName(), mVenue.getAddress(), mVenue.getCity(),
                mVenue.getState(), mVenue.getZip());
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareString.trim());

        mShareActionProvider.setShareIntent(shareIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.menu_item_share) {
            Toast.makeText(getActivity(), "Share", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
