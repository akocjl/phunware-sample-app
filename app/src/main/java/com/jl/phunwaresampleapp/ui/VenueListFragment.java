package com.jl.phunwaresampleapp.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.jl.phunwaresampleapp.R;
import com.jl.phunwaresampleapp.model.Venue;
import com.jl.phunwaresampleapp.rest.DataManager;

public class VenueListFragment extends ListFragment implements
        DataManager.VenueCallbacks {

    /**
     * The serialization bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    /**
     * The fragment's current callback object, which is notified of list item clicks.
     */
    private OnVenueSelectedListener mCallback;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface OnVenueSelectedListener {
        public void onVenueSelected(Venue item);
    }

    public VenueListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        // Download the list of venues, wait for the callback then populate the list
        DataManager.getInstance().getVenues(this);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof OnVenueSelectedListener)) {
            throw new IllegalStateException("Activity must implement " + OnVenueSelectedListener.class.getName());
        }

        mCallback = (OnVenueSelectedListener) activity;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Venue item = (Venue) l.getAdapter().getItem(position);
        mCallback.onVenueSelected(item);
    }

    @Override
    public void onVenuesDownloaded(List<Venue> venues) {
        setListAdapter(new VenueAdapter(getActivity(), venues));
    }

    @Override
    public void onError(String message) {
        // disable the progress indicator
        setListShown(true);

        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    public void setActivateOnItemClick(boolean activateOnItemClick) {
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    private class VenueAdapter extends ArrayAdapter<Venue> {

        private LayoutInflater mInflater;

        public VenueAdapter(Context context, List<Venue> objects) {
            super(context, 0, objects);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Venue item = getItem(position);
            ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.list_item_venue, parent, false);
                holder = new ViewHolder();
                holder.nameField = (TextView) convertView.findViewById(R.id.nameField);
                holder.addressField = (TextView) convertView.findViewById(R.id.addressField);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.nameField.setText(item.getName());
            holder.addressField.setText(item.getFullAddress(getActivity()));

            return convertView;
        }

        private class ViewHolder {
            TextView nameField;
            TextView addressField;
        }
    }
}
