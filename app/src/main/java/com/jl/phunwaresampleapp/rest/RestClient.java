package com.jl.phunwaresampleapp.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.jl.phunwaresampleapp.rest.service.BarService;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.converter.GsonConverter;

public class RestClient {

    private static final String BASE_URL = "https://s3.amazonaws.com/jon-hancock-phunware";
    private static BarService barService;

    public RestClient() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss Z")
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setConverter(new GsonConverter(gson))
                .setErrorHandler(new CustomErrorHandler())
                //.setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        barService = restAdapter.create(BarService.class);
    }

    public BarService getBarService() {
        return barService;
    }

    private class CustomErrorHandler implements ErrorHandler {

        @Override
        public Throwable handleError(RetrofitError cause) {
            String errorDescription = null;

            switch (cause.getKind()) {
                case HTTP:
                    errorDescription = String.valueOf(cause.getResponse().getStatus());
                    break;
                case NETWORK:
                    errorDescription = "No internet connection";
                    break;
            }

            if (errorDescription != null)
                return new Exception(errorDescription);

            return cause;
        }
    }
}
