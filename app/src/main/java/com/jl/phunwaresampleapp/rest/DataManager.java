package com.jl.phunwaresampleapp.rest;

import java.util.List;

import com.jl.phunwaresampleapp.model.Venue;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DataManager {

    private static DataManager instance;
    private RestClient mRestClient;

    private DataManager(){
        mRestClient = new RestClient();
    }

    public static DataManager getInstance() {
        if (instance == null)
            instance = new DataManager();

        return instance;
    }

    public void getVenues(final VenueCallbacks vc) {
        mRestClient.getBarService().getVenueList(new Callback<List<Venue>>() {
            @Override
            public void success(List<Venue> venues, Response response) {
                vc.onVenuesDownloaded(venues);
            }

            @Override
            public void failure(RetrofitError error) {
                vc.onError(error.getLocalizedMessage());
            }
        });

    }

    public interface VenueCallbacks {
        public void onVenuesDownloaded(List<Venue> venues);
        public void onError(String message);
    }
}
