package com.jl.phunwaresampleapp.rest.service;

import java.util.List;

import com.jl.phunwaresampleapp.model.Venue;
import retrofit.Callback;
import retrofit.http.GET;

public interface BarService {

    @GET("/nflapi-static.json")
    void getVenueList(Callback<List<Venue>> cb);

}

