package com.jl.phunwaresampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScheduleItem {

    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss Z");

    private static final SimpleDateFormat FORMATTER_DAY_DATE = new SimpleDateFormat(
            "EEEEE M/d");

    private static final SimpleDateFormat FORMATTER_HOUR = new SimpleDateFormat(
            "h:mma");

    private static final SimpleDateFormat FORMATTER_DAY_MONTH_YEAR = new SimpleDateFormat(
            "ddMMyyyy");

    @SerializedName("end_date")
    @Expose
    private Date mEndDate;

    @SerializedName("start_date")
    @Expose
    private Date mStartDate;

    public ScheduleItem() {

    }

    public ScheduleItem(Date startDate, Date endDate) {
        mStartDate = startDate;
        mEndDate = endDate;
    }

    public ScheduleItem(String startDate, String endDate) throws ParseException {
        mStartDate = FORMATTER.parse(startDate);
        mEndDate = FORMATTER.parse(endDate);
    }

    public Date getStartDate() {
        return mStartDate;
    }

    public void setStartDate(Date startDate) {
        mStartDate = startDate;
    }

    public Date getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Date endDate) {
        mEndDate = endDate;
    }

    public void setStartDate(String startDate) throws ParseException {
        mStartDate = FORMATTER.parse(startDate);
    }

    public void setEndDate(String endDate) throws ParseException {
        mEndDate = FORMATTER.parse(endDate);
    }

    public String getStartDateString() {
        return FORMATTER.format(mStartDate);
    }

    public String getEndDateString() {
        return FORMATTER.format(mEndDate);
    }

    public String getSchedule() {
        StringBuffer sb = new StringBuffer();
        sb.append(FORMATTER_DAY_DATE.format(mStartDate));
        sb.append(" ");
        sb.append(FORMATTER_HOUR.format(mStartDate));
        sb.append(" to ");

        if (!FORMATTER_DAY_MONTH_YEAR.format(mStartDate)
                .equals(FORMATTER_DAY_MONTH_YEAR.format(mEndDate))) {
            sb.append(FORMATTER_DAY_DATE.format(mEndDate));
            sb.append(" ");
        }

        sb.append(FORMATTER_HOUR.format(mEndDate));

        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof ScheduleItem) {
            result = mStartDate.equals(((ScheduleItem) o).getStartDate())
                    && mEndDate.equals(((ScheduleItem) o).getEndDate());
        }
        return result;
    }

    @Override
    public int hashCode() {
        String s = getStartDateString() + getEndDateString();
        return s.hashCode();
    }

}